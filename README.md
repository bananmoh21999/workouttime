# Workout Time!

Workout Time! is a cross-platform app for tracking timed workout routines.

## Installation

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" height="75" alt="Get it on F-Droid">](https://f-droid.org/packages/es.ideotec.workouttime/)

## Usage

The app comes with two predefined workouts. Tapping on a workout will run it, while tapping on the "⌄" symbol on the right of the workout will show its exercises. Each exercise has a preparation/rest time before it (show on the left), and a duration (shown on the right). Tapping anywhere on the list of exercises will go to the workout editing page, where you can modify each exercise (change its preparation time, name, or duration) as well as reorder, delete, and add new exercises.

When a workout is running there is a progress bar at the top showing the current point in the workout, with the time elapsed on the left and the time remaining (indicated with a -) on the right. In between these two times are a series of dots that represent the exercises in the workout, with the current exercise highlighted in blue. Below this is the name of the current exercise, and below that a stopwatch showing the current progress in that exercise, with the remaining time in the middle. At the bottom of the screen is the name of the next exercise.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)