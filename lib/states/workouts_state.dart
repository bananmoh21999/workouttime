import 'package:equatable/equatable.dart';
import '../models/workout.dart';

abstract class WorkoutsState extends Equatable {
  final List<Workout> workouts;
  WorkoutsState(this.workouts);
}

class WorkoutsInitial extends WorkoutsState {
  WorkoutsInitial() : super([]);

  @override
  List<Object> get props => [workouts];
}

class WorkoutsLoaded extends WorkoutsState {
  WorkoutsLoaded(List<Workout> workouts) : super(workouts);

  @override
  List<Object> get props => [workouts];
}
