import 'package:equatable/equatable.dart';
import '../models/workout.dart';

abstract class WorkoutState extends Equatable {}

class WorkoutInitial extends WorkoutState {
  @override
  List<Object> get props => [];
}

class WorkoutInProgress extends WorkoutState {
  final Workout workout;
  final int elapsed;
  WorkoutInProgress(this.workout, this.elapsed);

  @override
  List<Object> get props => [workout, elapsed];
}

class WorkoutEditing extends WorkoutState {
  final Workout workout;
  WorkoutEditing(this.workout);

  @override
  List<Object> get props => [workout];
}
