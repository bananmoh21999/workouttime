import 'package:flutter/foundation.dart';
import 'package:equatable/equatable.dart';

class Exercise extends Equatable {
  final int prelude;
  final String title;
  final int duration;
  final int index;
  final int startTime;

  Exercise(
      {@required this.prelude,
      @required this.title,
      @required this.duration,
      this.index,
      this.startTime});

  factory Exercise.fromJson(
          Map<String, dynamic> json, int index, int startTime) =>
      Exercise(
          prelude: json['prelude'] as int,
          title: json['title'] as String,
          duration: json['duration'] as int,
          index: index,
          startTime: startTime);

  Map<String, dynamic> toJson() =>
      {'title': title, 'prelude': prelude, 'duration': duration};

  @override
  List<Object> get props => [title, prelude, duration, index, startTime];

  @override
  bool get stringify => true;
}
