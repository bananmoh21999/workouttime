import 'package:flutter/foundation.dart';
import 'package:equatable/equatable.dart';
import 'exercise.dart';

class Workout extends Equatable {
  final String title;
  final List<Exercise> exercises;

  Workout({@required this.title, @required this.exercises});

  factory Workout.fromJson(Map<String, dynamic> json) {
    List<Exercise> exercises = [];
    int index = 0;
    int startTime = 0;
    (json['exercises'] as Iterable).forEach((ex) {
      exercises.add(Exercise.fromJson(ex, index, startTime));
      index++;
      startTime += exercises.last.prelude + exercises.last.duration;
    });
    return Workout(title: json['title'] as String, exercises: exercises);
  }

  Map<String, dynamic> toJson() => {'title': title, 'exercises': exercises};

  int getTotal() =>
      this.exercises.fold(0, (prev, ex) => prev + ex.duration + ex.prelude);

  Exercise getCurrentExercise(int elapsed) =>
      this.exercises.lastWhere((ex) => ex.startTime <= elapsed);

  @override
  List<Object> get props => [title, exercises];

  @override
  bool get stringify => true;
}
