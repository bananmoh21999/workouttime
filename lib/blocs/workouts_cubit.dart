import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import '../models/exercise.dart';
import '../models/workout.dart';
import '../states/workouts_state.dart';

class WorkoutsCubit extends HydratedCubit<WorkoutsState> {
  WorkoutsCubit() : super(WorkoutsInitial());

  getWorkouts() async {
    final List<Workout> workouts = [];
    final workoutsJson =
        jsonDecode(await rootBundle.loadString('assets/workouts.json'));
    (workoutsJson as Iterable)
        .forEach((el) => workouts.add(Workout.fromJson(el)));
    emit(WorkoutsLoaded(workouts));
  }

  saveWorkout(Workout workout) {
    Workout newWorkout = Workout(title: workout.title, exercises: []);
    int index = 0;
    int startTime = 0;
    workout.exercises.forEach((ex) {
      newWorkout.exercises.add(Exercise(
          prelude: ex.prelude,
          title: ex.title,
          duration: ex.duration,
          index: index,
          startTime: startTime));
      index++;
      startTime += ex.prelude + ex.duration;
    });
    List<Workout> newWorkouts = state.workouts
        .map((el) => (el.title == workout.title) ? newWorkout : el)
        .toList();
    emit(WorkoutsLoaded(newWorkouts));
  }

  addWorkout(String title) {
    state.workouts.add(Workout(title: title, exercises: []));
    emit(WorkoutsLoaded(state.workouts));
  }

  @override
  WorkoutsState fromJson(Map<String, dynamic> json) {
    List<Workout> workouts = [];
    json['workouts'].forEach((el) => workouts.add(Workout.fromJson(el)));
    return WorkoutsLoaded(workouts);
  }

  @override
  Map<String, dynamic> toJson(WorkoutsState state) {
    if (state is WorkoutsLoaded) {
      var json = {'workouts': []};
      state.workouts
          .forEach((workout) => json['workouts'].add(workout.toJson()));
      return json;
    } else
      return null;
  }
}
