import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wakelock/wakelock.dart';
import '../models/workout.dart';
import '../states/workout_state.dart';

class WorkoutCubit extends Cubit<WorkoutState> {
  WorkoutCubit() : super(WorkoutInitial());

  Timer _timer;

  startWorkout(Workout workout) {
    Wakelock.enable();
    emit(WorkoutInProgress(workout, 0));
    _timer = new Timer.periodic(new Duration(seconds: 1), (_) {
      WorkoutInProgress wip = state as WorkoutInProgress;
      if (wip.elapsed < wip.workout.getTotal())
        emit(WorkoutInProgress(wip.workout, wip.elapsed + 1));
      else {
        Wakelock.disable();
        _timer.cancel();
        emit(WorkoutInitial());
      }
    });
  }

  editWorkout(Workout workout) {
    emit(WorkoutEditing(workout));
  }

  goHome() {
    Wakelock.disable();
    _timer?.cancel();
    emit(WorkoutInitial());
  }
}
