import 'package:flutter/services.dart';
import 'package:soundpool/soundpool.dart';

String formatTime(int seconds, bool pad) {
  return (pad)
      ? (seconds / 60).floor().toString() +
          ":" +
          (seconds % 60).toString().padLeft(2, "0")
      : (seconds > 59)
          ? (seconds / 60).floor().toString() +
              ":" +
              (seconds % 60).toString().padLeft(2, "0")
          : seconds.toString();
}

class SoundPlayer {
  static Soundpool _soundpool = Soundpool(streamType: StreamType.alarm);
  static int _beepId;
  static int _dingId;
  static int _applauseId;

  static void loadSounds() async {
    _beepId = await rootBundle
        .load("assets/beep.oga")
        .then((ByteData soundData) => _soundpool.load(soundData));
    _dingId = await rootBundle
        .load("assets/ding.oga")
        .then((ByteData soundData) => _soundpool.load(soundData));
    _applauseId = await rootBundle
        .load("assets/applause.oga")
        .then((ByteData soundData) => _soundpool.load(soundData));
  }

  static void playBeep() => _soundpool.play(_beepId);
  static void playDing() => _soundpool.play(_dingId);
  static void playApplause() => _soundpool.play(_applauseId);
}
