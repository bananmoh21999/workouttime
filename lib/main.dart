import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:workouttime/helpers.dart';
import 'pages/workout_page.dart';
import 'blocs/workout_cubit.dart';
import 'states/workout_state.dart';
import 'blocs/workouts_cubit.dart';
import 'pages/edit_page.dart';
import 'pages/home_page.dart';
import 'states/workouts_state.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  HydratedBloc.storage = await HydratedStorage.build(
      storageDirectory: await getApplicationDocumentsDirectory());
  SoundPlayer.loadSounds();
  runApp(WorkoutTime());
}

class WorkoutTime extends StatelessWidget {
  @override
  Widget build(BuildContext context) => MaterialApp(
      title: 'Workout Time!',
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      theme: ThemeData(primarySwatch: Colors.blue),
      home: MultiBlocProvider(
          providers: [
            BlocProvider<WorkoutsCubit>(create: (BuildContext context) {
              WorkoutsCubit workoutsCubit = WorkoutsCubit();
              if (workoutsCubit.state is WorkoutsInitial)
                workoutsCubit.getWorkouts();
              return workoutsCubit;
            }),
            BlocProvider<WorkoutCubit>(
                create: (BuildContext context) => WorkoutCubit())
          ],
          child: BlocBuilder<WorkoutCubit, WorkoutState>(
              builder: (context, state) {
            if (state is WorkoutInProgress)
              return WorkoutPage();
            else if (state is WorkoutEditing)
              return EditPage();
            else
              return HomePage();
          })));
}
