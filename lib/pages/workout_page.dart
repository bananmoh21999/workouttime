import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dots_indicator/dots_indicator.dart';
import '../models/exercise.dart';
import '../helpers.dart';
import '../models/workout.dart';
import '../blocs/workout_cubit.dart';
import '../states/workout_state.dart';

class WorkoutPage extends StatelessWidget {
  Map<String, dynamic> _getStats(WorkoutInProgress wip) {
    Workout workout = wip.workout;
    int workoutTotal = workout.getTotal();
    int workoutElapsed = wip.elapsed;
    Exercise exercise = workout.getCurrentExercise(workoutElapsed);
    int exerciseElapsed = workoutElapsed - exercise.startTime;
    int exerciseRemaining = exercise.prelude - exerciseElapsed;
    bool isPrelude = exerciseElapsed < exercise.prelude;
    int exerciseTotal = isPrelude ? exercise.prelude : exercise.duration;
    if (!isPrelude) {
      exerciseElapsed -= exercise.prelude;
      exerciseRemaining += exercise.duration;
    }
    return {
      "workoutTitle": workout.title,
      "workoutProgress": workoutElapsed / workoutTotal,
      "workoutElapsed": workoutElapsed,
      "totalExercises": workout.exercises.length,
      "currentExerciseIndex": exercise.index.toDouble(),
      "workoutRemaining": workoutTotal - workoutElapsed,
      "isPrelude": isPrelude,
      "currentExercise": exercise.title,
      "exerciseProgress": exerciseElapsed / exerciseTotal,
      "exerciseRemaining": exerciseRemaining,
      "nextExercise": (exercise.index < workout.exercises.length - 1)
          ? workout.exercises[exercise.index + 1].title
          : null
    };
  }

  @override
  Widget build(BuildContext context) =>
      BlocConsumer<WorkoutCubit, WorkoutState>(listener: (context, state) {
        final stats = _getStats(state as WorkoutInProgress);
        if (stats['exerciseRemaining'] == 0)
          SoundPlayer.playApplause();
        else if (stats['exerciseProgress'] == 0)
          SoundPlayer.playDing();
        else if (stats['exerciseRemaining'] <= 3) SoundPlayer.playBeep();
      }, builder: (context, state) {
        final stats = _getStats(state as WorkoutInProgress);
        return WillPopScope(
            onWillPop: () => BlocProvider.of<WorkoutCubit>(context).goHome(),
            child: Scaffold(
                appBar: AppBar(
                    leading: BackButton(
                        onPressed: () =>
                            BlocProvider.of<WorkoutCubit>(context).goHome()),
                    title: Text(stats['workoutTitle'])),
                body: Container(
                    padding: EdgeInsets.all(32),
                    child: Column(children: [
                      LinearProgressIndicator(
                          backgroundColor: Colors.blue[100],
                          minHeight: 10,
                          value: stats['workoutProgress']),
                      Padding(
                          padding: EdgeInsets.only(top: 10),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(formatTime(stats['workoutElapsed'], true),
                                    style: TextStyle(fontSize: 16)),
                                DotsIndicator(
                                    dotsCount: stats['totalExercises'],
                                    position: stats['currentExerciseIndex']),
                                Text(
                                    '-' +
                                        formatTime(
                                            stats['workoutRemaining'], true),
                                    style: TextStyle(fontSize: 16))
                              ])),
                      Spacer(),
                      if (stats['isPrelude'])
                        Text('${AppLocalizations.of(context).getReadyFor}:',
                            style: TextStyle(color: Colors.red, fontSize: 18)),
                      Text(stats['currentExercise'],
                          style: TextStyle(
                              fontSize: 24, fontWeight: FontWeight.bold)),
                      Spacer(),
                      Stack(alignment: const Alignment(0, 0), children: [
                        Center(
                            child: SizedBox(
                                height: 220,
                                width: 220,
                                child: CircularProgressIndicator(
                                    backgroundColor: stats['isPrelude']
                                        ? Colors.red[50]
                                        : Colors.blue[50],
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        stats['isPrelude']
                                            ? Colors.red
                                            : Colors.blue),
                                    strokeWidth: 25,
                                    value: stats['exerciseProgress']))),
                        Center(
                          child: SizedBox(
                              height: 300,
                              width: 300,
                              child: Padding(
                                  padding: EdgeInsets.only(bottom: 20),
                                  child: Image.asset('stopwatch.png'))),
                        ),
                        SizedBox(
                            height: 200,
                            width: 200,
                            child: Center(
                                child: Text(
                                    formatTime(
                                        stats['exerciseRemaining'], false),
                                    style: TextStyle(
                                        fontSize: 50,
                                        fontWeight: FontWeight.bold))))
                      ]),
                      Spacer(),
                      Text(
                          (stats['nextExercise'] != null)
                              ? "${AppLocalizations.of(context).nextExercise}: ${stats['nextExercise']}"
                              : "${AppLocalizations.of(context).almostDone}",
                          style: TextStyle(fontSize: 18))
                    ]))));
      });
}
