import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../helpers.dart';
import '../blocs/workout_cubit.dart';
import '../blocs/workouts_cubit.dart';
import '../states/workouts_state.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _textFieldController = TextEditingController();
  WorkoutsCubit _workoutsCubit;
  WorkoutCubit _workoutCubit;

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(title: Text("Workout Time!")),
      body:
          BlocBuilder<WorkoutsCubit, WorkoutsState>(builder: (context, state) {
        _workoutsCubit = BlocProvider.of<WorkoutsCubit>(context);
        _workoutCubit = BlocProvider.of<WorkoutCubit>(context);
        return SingleChildScrollView(
            child: Container(
                child: ExpansionPanelList.radio(
                    children: state.workouts
                        .map((workout) => ExpansionPanelRadio(
                            value: workout,
                            headerBuilder: (BuildContext context, bool isExpanded) => ListTile(
                                title: Text(workout.title),
                                trailing:
                                    Text(formatTime(workout.getTotal(), true)),
                                onTap: () =>
                                    _workoutCubit.startWorkout(workout)),
                            body: InkWell(
                                onTap: () => _workoutCubit.editWorkout(workout),
                                child: (workout.exercises.length > 0)
                                    ? ListView.builder(
                                        shrinkWrap: true,
                                        itemCount: workout.exercises.length,
                                        itemBuilder: (BuildContext context,
                                                int index) =>
                                            ListTile(
                                                visualDensity: VisualDensity(
                                                    horizontal: 0,
                                                    vertical: VisualDensity
                                                        .minimumDensity),
                                                leading: Text(formatTime(
                                                    workout.exercises[index]
                                                        .prelude,
                                                    true)),
                                                title: Text(workout.exercises[index].title),
                                                trailing: Text(formatTime(workout.exercises[index].duration, true))))
                                    : Column(children: [
                                        Padding(
                                            padding:
                                                EdgeInsets.only(bottom: 20),
                                            child: Text(
                                                AppLocalizations.of(context)
                                                    .addExercises,
                                                style: TextStyle(
                                                    color: Colors.blue)))
                                      ]))))
                        .toList())));
      }),
      floatingActionButton: FloatingActionButton(
          onPressed: () => showDialog(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                      content: TextField(
                          controller: _textFieldController,
                          decoration: InputDecoration(
                              labelText:
                                  AppLocalizations.of(context).workoutName)),
                      actions: [
                        TextButton(
                            onPressed: () {
                              _workoutsCubit
                                  .addWorkout(_textFieldController.text);
                              setState(() => Navigator.pop(context));
                              _workoutCubit.editWorkout(
                                  _workoutsCubit.state.workouts.last);
                            },
                            child: Text(AppLocalizations.of(context).create))
                      ])),
          child: Icon(Icons.add)));
}
