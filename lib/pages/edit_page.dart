import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:numberpicker/numberpicker.dart';
import '../helpers.dart';
import '../models/exercise.dart';
import '../blocs/workouts_cubit.dart';
import '../models/workout.dart';
import '../states/workout_state.dart';
import '../blocs/workout_cubit.dart';

class EditPage extends StatefulWidget {
  @override
  _EditPageState createState() => _EditPageState();
}

class _EditPageState extends State<EditPage> {
  int _editing;
  int _prelude;
  TextEditingController _title;
  int _duration;
  int _index;

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<WorkoutCubit, WorkoutState>(builder: (context, state) {
        Workout workout = (state as WorkoutEditing).workout;
        return WillPopScope(
            onWillPop: () => BlocProvider.of<WorkoutCubit>(context).goHome(),
            child: Scaffold(
                appBar: AppBar(
                    leading: BackButton(
                        onPressed: () =>
                            BlocProvider.of<WorkoutCubit>(context).goHome()),
                    title: Text(
                        '${AppLocalizations.of(context).edit} ${workout.title}'),
                    actions: [
                      IconButton(
                          icon: Icon(Icons.save),
                          onPressed: () {
                            BlocProvider.of<WorkoutsCubit>(context)
                                .saveWorkout(workout);
                            BlocProvider.of<WorkoutCubit>(context).goHome();
                          })
                    ]),
                body: ListView.builder(
                    itemCount: workout.exercises.length,
                    itemBuilder: (BuildContext context, int index) {
                      Exercise exercise = workout.exercises[index];
                      return (_editing == index)
                          ? Column(children: [
                              Row(children: [
                                Expanded(
                                    child: NumberPicker(
                                        itemHeight: 30,
                                        value: _prelude,
                                        minValue: 0,
                                        maxValue: 600,
                                        textMapper: (strVal) => formatTime(
                                            int.parse(strVal), false),
                                        onChanged: (value) =>
                                            setState(() => _prelude = value))),
                                Expanded(
                                    flex: 3,
                                    child: TextField(
                                        textAlign: TextAlign.center,
                                        controller: _title)),
                                Expanded(
                                    child: NumberPicker(
                                        itemHeight: 30,
                                        value: _duration,
                                        minValue: 1,
                                        maxValue: 600,
                                        textMapper: (strVal) => formatTime(
                                            int.parse(strVal), false),
                                        onChanged: (value) =>
                                            setState(() => _duration = value)))
                              ]),
                              ButtonBar(
                                  alignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    IconButton(
                                        icon: Icon(Icons.arrow_upward),
                                        onPressed: (_editing == 0)
                                            ? null
                                            : () => setState(() {
                                                  workout.exercises.insert(
                                                      _editing - 1,
                                                      workout.exercises
                                                          .removeAt(_editing));
                                                  _editing--;
                                                })),
                                    IconButton(
                                        icon: Icon(Icons.arrow_downward),
                                        onPressed: (_editing ==
                                                workout.exercises.length - 1)
                                            ? null
                                            : () => setState(() {
                                                  workout.exercises.insert(
                                                      _editing + 1,
                                                      workout.exercises
                                                          .removeAt(_editing));
                                                  _editing++;
                                                })),
                                    IconButton(
                                        icon: Icon(Icons.delete),
                                        onPressed: () {
                                          workout.exercises.removeAt(_editing);
                                          setState(() => _editing = null);
                                        }),
                                    Text("|"),
                                    IconButton(
                                        icon: Icon(Icons.cancel_outlined),
                                        color: Colors.red,
                                        onPressed: () => setState(() {
                                              workout.exercises.insert(
                                                  _index,
                                                  workout.exercises
                                                      .removeAt(_editing));
                                              _editing = null;
                                            })),
                                    IconButton(
                                        icon: Icon(Icons.check_circle_outline),
                                        color: Colors.green,
                                        onPressed: () => setState(() {
                                              workout.exercises.replaceRange(
                                                  _editing, _editing + 1, [
                                                Exercise(
                                                    prelude: _prelude,
                                                    title: _title.text,
                                                    duration: _duration)
                                              ]);
                                              _editing = null;
                                            }))
                                  ])
                            ])
                          : ListTile(
                              leading: Text(formatTime(exercise.prelude, true)),
                              title: Text(exercise.title),
                              trailing:
                                  Text(formatTime(exercise.duration, true)),
                              onTap: () {
                                setState(() {
                                  _editing = index;
                                  _prelude = exercise.prelude;
                                  _title = TextEditingController(
                                      text: exercise.title);
                                  _duration = exercise.duration;
                                  _index = index;
                                });
                              });
                    }),
                floatingActionButton: (_editing != null)
                    ? null
                    : FloatingActionButton(
                        onPressed: () => setState(() {
                              workout.exercises.add(Exercise(
                                  prelude: 0, title: '', duration: 60));
                              _editing = workout.exercises.length - 1;
                              _prelude = 0;
                              _title = TextEditingController(text: '');
                              _duration = 60;
                              _index = workout.exercises.length - 1;
                            }),
                        child: Icon(Icons.add))));
      });
}
